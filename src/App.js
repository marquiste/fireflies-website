import React, { Component } from 'react';
import './App.css';
import Header from './components/header/header.jsx';
import Menu from './components/menu';
import Footer from './components/footer/footer.jsx';
import Routes from './routes';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Menu />
        <main className="container" role="main">
          <Routes />
        </main>
        <Footer />
      </div>
    );
  }
}

export default App;
