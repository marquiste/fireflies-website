import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Index from './pages/index';
import Packs from './pages/packs';
import Contact from './pages/contact';
import Faqs from './pages/faqs';

const Routes = (props) => (
  <BrowserRouter {...props}>
    <div>
      <Route exact path="/" component={Index} />
      <Route exact path="/packs" component={Packs} />
      <Route exact path="/contact" component={Contact} />
      <Route exact path="/faqs" component={Faqs} />
    </div>
  </BrowserRouter>
);

export default Routes;
