import React, { Component } from 'react';
import Channel from '../../components/channel';
import './index.css';

class Index extends Component {
  render() {
    return (
      <div className="pages-index">
        <div className="container">
          <h1>Channels</h1>
          <div className="channels">
            <Channel channel="fireflies-screensaver-free" />
            <Channel channel="fireflies-screensaver" />
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
