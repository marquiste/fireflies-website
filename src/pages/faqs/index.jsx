import React, { Component } from 'react';
import './faqs.css';

export default class Faqs extends Component {
  render() {
    return (
      <div className="pages-faqs">
        <div className="container">
          <h1>Frequently Asked Questions</h1>
          <h2>How do I add the channel to my player?</h2>
          <div>
            <p><strong>Fireflies Screensaver:</strong> Go to <a target="_blank" rel="noopener noreferrer" href="https://owner.roku.com/add/firefliessaver">https://owner.roku.com/add/firefliessaver</a> to add this channel to your Roku player. If you are not currently logged into your Roku account, you will be asked to log in.</p>
            <p><strong>Fireflies Screensaver Free:</strong> Go to <a target="_blank" rel="noopener noreferrer" href="https://owner.roku.com/add/firefliessaverfree">https://owner.roku.com/add/firefliessaverfree</a> to add this channel to your Roku player. If you are not currently logged into your Roku account, you will be asked to log in.</p>
            <p>Alternatively, you can browse to the channel using the Channel Store on your Roku player or on the <a target="_blank" rel="noopener noreferrer" href="http://www.roku.com/channels/#!browse/screensavers-and-apps/by-popular">Roku site</a> and add the channel that way. It will be under the Screensaver &amp; Apps section.</p>
          </div>
          <h2>How much does Fireflies Screensaver Free cost?</h2>
          <div>
            <p>It’s all in the title; Fireflies Screensaver Free is <strong>free</strong>. There is no cost to download, install, and use this screensaver for as long as you like. There are no third-party ads, forced previews, or limited usage restrictions.</p>
            <p>If you like the free screensaver, consider upgrading to Fireflies Screensaver for enhanced software at a low cost. You’ll be upgrading your experience and helping us to keep producing the content that you love.</p>
          </div>
          <h2>How much does Fireflies Screensaver cost?</h2>
          <div>
            <p>Fireflies Screensaver is a subscription based screensaver that costs $4.99/year. That’s about the cost of a double shot white mocha hold the whip for a full year of usage!&nbsp;With that you get updated content and features on a regular basis without having to pay anything extra. There are no hidden fees, no additional purchases, and no annoying advertising. Just enjoy the new content that keeps coming your way. And if for any reason you would like to discontinue the service, there are no penalties for cancelling.</p>
          </div>
          <h2>Where do I change the settings?</h2>
          <div>
            <p><strong>For software version 5.0 and up:</strong></p>
            <ul>
            <li>Press the Home button on your Roku Remote</li>
            <li>Scroll down and select Settings</li>
            <li>Scroll down and select Screensaver</li>
            <li>Press the right button to access your list of installed screensavers</li>
            <li>Scroll down to Fireflies Screensaver Free</li>
            <li>Press the right button to access the screensaver menu and select Custom Settings</li>
            </ul>
            <p><strong>For software versions prior to version 5.0:</strong></p>
            <ul>
            <li>Press the Home button on your Roku Remote</li>
            <li>Scroll to the far left on your channels list and select Roku Player Settings</li>
            <li>Scroll down and select Screensaver</li>
            <li>Use the left/right buttons to select Fireflies Screensaver Free</li>
            <li>Scroll down and select Custom Settings</li>
            </ul>
          </div>
          <h2>Why is my animation slow/stuttering?</h2>
          <div>
            <p>Some older Roku models may have trouble handling an animation density setting of <strong>dense</strong>. Try setting your animation density to <strong>normal</strong> or <strong>sparse</strong> to improve the performance of the screensaver.</p>
          </div>
          <h2>Why am I seeing the default Roku screensaver and not the Fireflies Screensaver?</h2>
          <div>
            <p>Some channels use a lot of memory. If you are using a channel that is memory intensive, there may not be enough memory for the Fireflies Screensaver to run. In this case the default screensaver is shown instead. This is most often seen when you are watching content and pause in the middle. Because much of the memory buffer is being used to store the stream, there isn’t a lot of room left for the screensaver to run.</p>
          </div>
          <h2>I have another question/problem.</h2>
          <div>
            <p>Please <a href="/contact">Contact Us</a> so that we may help you.</p>
          </div>
        </div>
      </div>
    );
  }
}
