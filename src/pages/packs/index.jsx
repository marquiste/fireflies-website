import React, { Component } from 'react';
import './packs.css';
import Gallery from '../../components/gallery';

export default class Packs extends Component {
  render() {
    return (
      <div className="pages-packs">
        <div className="container">
          <h1>Image Packs</h1>
          <h2>Cities</h2>
          <Gallery id="cities" />
          <h2>Flowers</h2>
          <Gallery id="flowers" />
          <h2>Nature</h2>
          <Gallery id="nature" />
          <h2>Oriental</h2>
          <Gallery id="oriental" />
          <h2>Tropical</h2>
          <Gallery id="tropical" />
        </div>
      </div>
    );
  }
}
