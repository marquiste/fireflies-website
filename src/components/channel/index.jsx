import React, { Component } from 'react';
import Markdown from 'react-markdown';
import './channel.css';
import Channels from './channels.json';
import Gallery from '../gallery';

export default class Channel extends Component {
  render() {
    let model = Channels[this.props.channel];
    return (
      <section className="components-channel">
        <section className="channel-overview">
          <h2>{model.title}</h2>
          <Markdown source={model.description} />
          {model.gallery?(
            <Gallery id={model.gallery} />
          ):''}
          {model.add_channel?(
            <p>
              <strong>Add channel link: </strong>
              <a
                href={model.add_channel}
                alt={"Add channel: "+model.title}
              >
                {model.add_channel}
              </a>
            </p>
          ):''}
        </section>
        <section className="channel-features">
          {model.features.map((feature, i) => {
            return (
              <Feature key={"feature-"+i} model={feature} />
            )
          }, this)}
        </section>
      </section>
    );
  }
}

class Feature extends Component {
  render() {
    let model = this.props.model;
    return (
      <section className="feature">
        <h3>{model.title}</h3>
        <ul>
        {model.list.map((item, i) => {
          return (
            <li key={"feature-item-"+i}>{item}</li>
          )
        }, this)}
        </ul>
      </section>
    )
  }
}
