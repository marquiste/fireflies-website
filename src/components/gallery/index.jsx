import React, { Component } from 'react';
import './gallery.css';
import Galleries from './gallery.json';
import Spinner from '../spinner';
import Lightbox from '../lightbox';

export default class Gallery extends Component {
  constructor(props) {
    super(props);

    this.state = {
        index: -1
    }
    this.TNAIL_HEIGHT = "84";
    this.TNAIL_WIDTH = "150";
  }

  hideSpotlight() {
    this.setState({index: -1});
  }
  showSpotlight(index) {
    this.setState({index: index});
  }

  render() {
    let id = this.props.id;
    let model = Galleries[id];
    let spotlight = (this.state.index>=0)?
      model[this.state.index]:
      undefined;

    return (
      <section className="components-gallery">
        {spotlight!==undefined?(
          <Lightbox
            onClose={this.hideSpotlight.bind(this)}
          >
            <Spotlight
              image={'/images/gallery/'+id+'/'+spotlight.spotlight}
              title={spotlight.title}
            />
          </Lightbox>
        ):''}
        <div className="gallery-thumbnails">
          {model.map((image, i) => {
            return (
              <div key={"gallery-thumbnail-"+i}
                className="gallery-thumbnail"
                onClick={this.showSpotlight.bind(this, i)}
              >
                <img
                  height={this.TNAIL_HEIGHT}
                  width={this.TNAIL_WIDTH}
                  src={process.env.PUBLIC_URL + '/images/gallery/'+id+'/'+image.thumbnail}
                  alt={image.title}
                />
              </div>
            )
          }, this)}
        </div>
      </section>
    );
  }
}

class Spotlight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      styles: {
        maxHeight: 'auto',
        maxWidth: 'auto'
      }
    };
  }
  
  render() {
    return(
      <div  className="gallery-spotlight-content">
        {(this.state.loading)?(
            <div className="loading">
              <Spinner />
              <img
                onLoad={this.imageLoaded.bind(this)}
                className="image-preload"
                src={process.env.PUBLIC_URL + this.props.image}
                alt={this.props.title}
              />
            </div>
        ):(
          <section className="gallery-spotlight">
            <div style={this.state.styles} className={"gallery-spotlight-image"}>
              <img
                src={process.env.PUBLIC_URL + this.props.image}
                alt={this.props.title}
              />
            </div>
            <label className={"gallery-spotlight-title"}>
              {this.props.title}
            </label>
          </section>
        )}
      </div>
    );
  }

  handleResize() {
    let width = window.innerWidth*.6;
    let height = window.innerHeight*.6;
    let styles = {};
    if(width > height) {
      styles = {...this.state.styles, maxHeight: height, maxWidth: width};
    } else {
      styles = {...this.state.styles, maxHeight: 'auto', maxWidth: 'auto'};
    }
    this.setState({styles, styles});

  }

  imageLoaded() {
    this.setState({loading: false});
  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize.bind(this));
  }
}
