import React, { Component } from 'react';
import logo from './images/fireflies-logo.png';
import './header.css';

class Header extends Component {
  render() {
    return (
      <header className="components-header">
        <div role="banner" className="container">
          <a
            href="/"
            title="home"
            tabIndex="1"
          >
          <img className="logo"
            src={logo}
            alt="Fireflies"
          />
          </a>
        </div>
      </header>
    );
  }
}

export default Header;
