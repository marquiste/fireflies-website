import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Spinner from '../spinner';

describe('<Spinner />', () => {
  const component = shallow(<Spinner />);

  if('Renders', () => {
    expect(component.find('.component-spinner')).to.have.length(1);
  });

  it('Renders eight circles', () => {
    expect(component.find('.circularG')).to.have.length(8);
  });

  it('Renders a caption', () => {
    expect(component.find('figcaption')).to.have.length(1);
    expect(component.find('figcaption').text()).to.be.a('string');
  });
});
