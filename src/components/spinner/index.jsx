import React, { Component } from 'react';
import './spinner.css';

export default class Spinner extends Component {
  render() {
    return (
      <figure className="component-spinner">
      	<div id="circularG_1" className="circularG"></div>
      	<div id="circularG_2" className="circularG"></div>
      	<div id="circularG_3" className="circularG"></div>
      	<div id="circularG_4" className="circularG"></div>
      	<div id="circularG_5" className="circularG"></div>
      	<div id="circularG_6" className="circularG"></div>
      	<div id="circularG_7" className="circularG"></div>
      	<div id="circularG_8" className="circularG"></div>
        <figcaption>Loading</figcaption>
      </figure>
    );
  }
};
