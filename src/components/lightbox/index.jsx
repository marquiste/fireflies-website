import React, { Component } from 'react';
import './lightbox.css';

export default class Lightbox extends Component {
  render() {
    return (
      <div className="components-lightbox">
        <button className="close" title="close"
          onClick={this.props.onClose}>
          <span className="icon-cross" />
        </button>
        <div className="content">
          {this.props.children}
        </div>
      </div>
    );
  }
};
