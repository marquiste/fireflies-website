import React, { Component } from 'react';
import './footer.css';

class Footer extends Component {
  render() {
    return (
      <footer role="contentinfo" className="components-footer">
          <div className="container content">
            <div className="copyright">
              &copy;2017 Marquiste, Inc.
            </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
