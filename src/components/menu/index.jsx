import React, { Component } from 'react';
import './menu.css';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [
        {
          url: '/',
          label: 'Home',
          icon: 'home'
        },
        {
          url: '/packs',
          label: 'Packs',
          icon: 'images'
        },
        {
          url: '/faqs',
          label: 'FAQs',
          icon: 'bubbles3'
        },
        {
          url: '/contact',
          label: 'Contact',
          icon: 'envelop'
        }
      ]
    }
  }

  render() {
    return (
      <div className="component-menu">
          <nav className="menu-list container">
          {this.state.menu.map((item, i) => {
            return (
              <a key={'menu-item-'+i}
                className="menu-list-item"
                href={item.url}
                title={item.label}
                tabIndex={i+1}
              >
                <span className={"icon icon-"+item.icon} />
                <span className="title">{item.label}</span>
              </a>
            )
          }, this)}
        </nav>
      </div>
    );
  }
}

export default Menu;
