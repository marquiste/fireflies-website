import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import Menu from '../menu';

describe('<Menu />', () => {
  const component = shallow(<Menu />);

  if('Renders', () => {
    expect(component.find('.component-menu')).to.have.length(1);
  });

  describe('Tabs', () => {
    const tabs = component.find('.menu-list-item');

    it('Renders menu tabs', () => {
      expect(tabs.exists()).to.equal(true);
    });

    it('Renders menu tabs with icons as first child', () => {
      tabs.forEach((node) => {
        expect(node.childAt(0).hasClass('icon')).to.equal(true);
      });
    });

    it('Renders menu tabs with text as second child', () => {
      tabs.forEach((node) => {
        expect(node.childAt(1).text()).to.be.a('string');
      });
    });

    it('Renders menu tabs as links', () => {
      tabs.forEach((node) => {
        expect(node.filterWhere((item) => {
          return item.prop('href') !== undefined;
        })).to.have.length(1);
      });
    });
  });
});
